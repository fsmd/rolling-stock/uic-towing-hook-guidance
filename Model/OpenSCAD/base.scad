/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A plate to get the jig handled
 */
module base() {
  length = 25;
  width = 20;

  thickness = 1;
  champfer = 0.8;

  hull() {
    translate([0, 0, -(thickness - champfer) / 2]) cube([length, width, thickness - champfer], true);
    translate([0, 0, -thickness / 2]) cube([length - 2 * champfer, width - 2 * champfer, thickness], true);
  }
}

base();
