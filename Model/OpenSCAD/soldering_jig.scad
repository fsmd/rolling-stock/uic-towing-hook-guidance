/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Valentin Funk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A jig to solder the towing-hook guidance (Zughakenführung) in H0
 */
module soldering_jig(hole_diameter = 0.3) {

  length = 10;
  width = 5;
  thickness = 1.5;

  archetype_distance_horizontal = 535;
  archetype_distance_vertical = 160;

  translate([0, 0, thickness / 2]) difference() {
    cube([length, width, thickness], true);
    _center_opening();
    _rods();
  }

  module _rods() {
    translate_x = archetype_distance_horizontal / 2 / 87;
    translate_y = archetype_distance_vertical / 2 / 87;
    edges = [
      [translate_x, translate_y],
      [translate_x, -translate_y],
      [-translate_x, translate_y],
      [-translate_x, -translate_y],
    ];

    for (i = edges) {
      translate(i) _rod();
    }

    module _rod() {
      cylinder(h = thickness * 1.1, d = hole_diameter, center = true);
    }
  }

  module _center_opening() {
    cube([4.5, 1.8, thickness * 1.1], true);
  }
}

// The wire is 0.3 mm thick. But 0.3 mm holes are not possible with the Elegoo Mars!
hole_diameter = 0.5;
soldering_jig(hole_diameter, $fn = 500);
