# Settings

**On Elegoo Mars // CHITUBOX**

## DruckWege Pigment Free

### Print

* Layer Height: 25 μm
* Exposure Time: 7.5 s
* Light-off Delay: 0 s
* Bottom Layer Count: 8
* Botom Exposure Time: 105 s
* Bottom Light-off Delay: 0 s

* Lifting Distance: 5 mm
* Lifting Speed: 100 mm/min
* Bottom Lift Distance: 5 mm
* Bottom Lift Speed: 90 mm/min
* Retract Speed: 150 mm/min

### Advanced

* Anti-aliasing: on
* Anit-aliasing Level: 4
